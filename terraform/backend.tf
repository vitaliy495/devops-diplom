terraform {
  backend "http" {
    address         = "https://gitlab.com/api/v4/projects/54841241/terraform/state/default.tfstate"
    lock_address    = "https://gitlab.com/api/v4/projects/54841241/terraform/state/default.tfstate/lock"
    unlock_address  = "https://gitlab.com/api/v4/projects/54841241/terraform/state/default.tfstate/lock"
    username        = "vitaliy495"
#    password        = "$GITLAB_ACCESS_TOKEN"
    lock_method     = "POST"
    unlock_method   = "DELETE"
    retry_wait_min  = 5
  }
}
