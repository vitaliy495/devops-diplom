#!/bin/bash
# Установка prometheus
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts && helm repo update
##### (пока без) создадим отдельный namespace по имени monitoring
######kubectl create namespace monitoring
# helm install monitoring prometheus-community/kube-prometheus-stack -n monitoring -f values.yaml
helm install my-prom prometheus-community/prometheus # -n monitoring
# Проверим, что прометеус установился и заработал
kubectl get pods -l "app.kubernetes.io/instance=my-prom"
# Установим кеширующий прокси trickster
helm repo add tricksterproxy https://helm.tricksterproxy.io && helm repo update
helm install trickster tricksterproxy/trickster --namespace default -f monitoring/trickster.yaml
sleep 20
# Убедимся, что под trickster перешел в состояние Running
kubectl get pods -l "app=trickster"
# Установим Grafana
kubectl apply -f monitoring/grafana.yaml
# Убедимся, что под Grafana перешел в состояние Running
sleep 10
kubectl get pods -l "app=grafana"
# Адрес на котором доступен сервис Grafana сохраним в файл grafana.url
export GRAFANA_IP=$(kubectl get service/grafana -o jsonpath='{.status.loadBalancer.ingress[0].ip}') && \
export GRAFANA_PORT=$(kubectl get service/grafana -o jsonpath='{.spec.ports[0].port}') && \
echo http://$GRAFANA_IP:$GRAFANA_PORT > grafana.url
# Логин и Пароль admin/admin
# Добавьте источник данных c типом Prometheus и следующими настройками
# Name — Prometheus ; URL — http://trickster:8480
# Импортируйте дашборд Kubernetes Deployment Statefulset Daemonset metrics,
# содержащий основные метрики Kubernetes. Укажите идентификатор дашборда (8588) при импорте
