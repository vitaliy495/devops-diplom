terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
#      version = "~> 0.99"
    }
    template = "~> 2.2.0"
  }
  required_version = ">=0.13"

# backend ycloud 
#  backend "s3" {
#    endpoint = "storage.yandexcloud.net"
#    bucket = "diplom-vitnek"
#    region = "ru-central1"
#    key = "terraform.tfstate"
#    skip_region_validation = true
#    skip_credentials_validation = true
#    dynamodb_endpoint = "https://docapi.serverless.yandexcloud.net/ru-central1/b1g8roqvnljdcrg08i45/etndqgdl7q6kbuje2ae2"
#    dynamodb_table = "table113"
#  }
}

provider "yandex" {
  service_account_key_file = "key.json"
  cloud_id  = var.yc_cloud_id
  folder_id = var.yc_folder_id
  zone      = "ru-central1-a"
}
